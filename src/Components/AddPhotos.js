import React, {Component} from 'react'

class AddPhotos extends Component {

    constructor(props) {
        super(props);
        this.submitPhoto = this.submitPhoto.bind(this);
    }

    submitPhoto(e) {
        e.preventDefault();
        const imgLink = e.target.elements.link.value;
        const imgDesc = e.target.elements.desc.value;
        const post = {
            id: Number(new Date()),
            description: imgDesc,
            src: imgLink
        };
        this.props.startAddingPost(post);
        this.props.routeHistory.push('/');
    }

    render() {
        return (
            <div>
                <div className='form'>
                    <form onSubmit={this.submitPhoto}>
                        <input type='text' placeholder='Link' name='link'/>
                        <input type='text' placeholder='Description' name='desc'/>
                        <button>Post</button>
                    </form>
                </div>
            </div>
        )
    }

}

export default AddPhotos;
