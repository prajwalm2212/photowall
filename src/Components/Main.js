import React, {Component} from 'react';
import PhotoWall from './PhotoWall'
import '../styles/stylesheet.css'
import {Route} from 'react-router-dom'
import AddPhotos from "./AddPhotos";
import AddComments from "./AddComments";
import {Link} from "react-router-dom";


class Main extends Component {

    componentDidMount() {
        this.props.startLoadingPosts();
        this.props.startGettingComments();
    }

    render() {
        return (
            <div>
                <Link  className='heading' to={'/'}><h1>PhotoWall</h1></Link>
                <Route exact path='/'
                       render={(params) => <PhotoWall {...this.props} {...params}/>}/>

                <Route path='/AddPhotos'
                       render={({history}) => <AddPhotos {...this.props} routeHistory={history}/>}/>

                <Route path='/AddComment/:id' render={(params) => <AddComments {...this.props} {...params}/>}/>
            </div>
        )
    }
}

export default Main;
