import React, {Component} from 'react'
import Photo from './Photo'
import Comment from './Comment'


class AddComments extends Component {
    render() {
        const {match, posts} = this.props;
        const id = Number(match.params.id);
        const post = posts.find((post) => post.id === id);
        return (
            <div className={'single-photo'}>
                <Photo post={post} {...this.props} index={id} key={id}/>
                <Comment postId={id} comments={this.props.comments} addComment={this.props.startAddingComment}/>
            </div>
        );
    }
}

export default AddComments;
