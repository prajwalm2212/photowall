import {bindActionCreators} from "redux";
import {connect} from 'react-redux'
import * as actionCreators from '../redux/actions'
import Main from './Main'
import {withRouter} from "react-router-dom";

/*
1. Map state to props
2. Match dispatch to props
3. connect
 */

function mapStateToProps(state, ownProps){
    return {
        posts: state.posts,
        comments: state.comments
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(actionCreators, dispatch);
}

const App = withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));

export default App;