import React, {Component} from 'react';
import Photo from './Photo'
import {Link} from 'react-router-dom'

class PhotoWall extends Component {
    render() {
        return (
            <div>
                <Link to='/AddPhotos'>
                    <img src='https://image.flaticon.com/icons/png/512/60/60740.png' alt='add photos'
                         className='chg-btn'/>
                </Link>
                <div className="photo-grid">
                    {this.props.posts.map((post, idx) => <Photo key={idx} post={post} index={idx}
                                                                {...this.props}/>)}
                </div>
            </div>
        )
    }
}

export default PhotoWall;