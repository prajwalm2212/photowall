import React, {Component} from 'react'

class Comment extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        const comment = e.target.elements.cmt.value;
        this.props.addComment(comment, this.props.postId);
        e.target.elements.cmt.value = '';
    }

    render() {
        return (
            <div className={'comment-form'}>
                {
                    this.props.comments[this.props.postId] ?
                        this.props.comments[this.props.postId].map((comment, idx) => {
                            return <div key={idx}><p>{comment}</p>
                                <hr/>
                            </div>;
                        }) : false
                }
                <form onSubmit={this.handleSubmit}>
                    <input type='text' placeholder={'comment'} name={'cmt'}/>
                    <hr/>
                </form>
            </div>
        );
    }
}

export default Comment;
