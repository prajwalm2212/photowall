import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class Photo extends Component {
    render() {
        const post = this.props.post;
        const comments = this.props.comments[post.id];
        return (
            <figure className="figure">
                <Link to={`/AddComment/${post.id}`}>
                    <img className="photo" src={post.src} alt={post.description}/>
                </Link>
                <div className="img-desc">
                    <figcaption><p>{this.props.post.description}</p></figcaption>
                    <div className="btn">
                        <button onClick={() => {
                            this.props.history.push('/');
                            this.props.startRemovingPost(this.props.index, post.id);
                        }}>Remove
                        </button>
                        <Link className='link-cmt' to={`/AddComment/${post.id}`}>
                            <img className='cmt-img' src='https://d30y9cdsu7xlg0.cloudfront.net/png/51904-200.png' alt='comment'/>
                            <div className='cmt-count'>{comments ? comments.length : 0}</div>
                        </Link>
                    </div>
                </div>
            </figure>
        )
    }
}

export default Photo;
