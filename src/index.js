import ReactDOM from 'react-dom'
import App from './Components/App'
import React from 'react'
import {BrowserRouter} from 'react-router-dom'
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './redux/reducers'

/*
Create a store and inject into main
 */

const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(<Provider
    store={store}><BrowserRouter><App/></BrowserRouter></Provider>, document.getElementById('root'));
