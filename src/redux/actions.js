import database from "../environment/config";

export function startAddingPost(post) {
    return (dispatch) => {
        return database.ref('posts')
            .update({[post.id]: post})
            .then(() => dispatch(addPost(post)))
            .catch((err) => console.log(err))
    }
}

export function startRemovingPost(index, postId) {
    return (dispatch) => {
        return database.ref(`posts/${postId}`)
            .remove()
            .then(() => {
                dispatch(removePost(index))
            })
            .catch((err) => console.log(err))
    }
}

export function startLoadingPosts() {
    return (dispatch) => {
        return database.ref('posts').once('value').then((snapshot) => {
            let posts = [];
            console.log(snapshot);
            snapshot.forEach((childSnapShot) => {
                posts.push(childSnapShot.val())
            });
            dispatch(addPosts(posts));
            console.log(posts)
        })
    }
}

export function startAddingComment(comment, postId) {

    return (dispatch) => {
        return database.ref('comments/' + postId)
            .push(comment)
            .then(() => {
                dispatch(addComment(comment, postId));
            })
    }

}

export function startGettingComments() {
    return (dispatch) => {
        return database.ref('comments')
            .once('value')
            .then((snapshot) => {
                let comments = {};
                snapshot.forEach((child) => {
                    comments[child.key] =  Object.values(child.val())
                });
                dispatch(loadComments(comments))
            })
    }
}

export function loadComments(comments) {
    return {
        type: 'LOAD_COMMENTS',
        comments
    }
}


export function addPosts(posts) {
    return {
        type: 'ADD_POSTS',
        posts
    }
}

export function removePost(index) {
    return {
        type: 'REMOVE_POST',
        index
    }
}

export function addPost(post) {
    return {
        type: 'ADD_POST',
        post
    }
}

export function addComment(comment, postId) {
    return {
        type: 'ADD_COMMENT',
        comment,
        postId
    }
}
