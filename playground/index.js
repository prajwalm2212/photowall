import React, {Component} from 'react';
import ReactDOM from 'react-dom';


const subjects = ['SE', 'TFCS', 'OS', 'DBMS'];
// React can create elements
const element = React.createElement('ol', null,
    subjects.map((sub, idx) => React.createElement('li', {key: idx}, sub))
);

// JSX allows use of JS in html
const element_jsx = (
    // div must be used to equate this to one block of html
    <div>
        <h1> Subjects </h1>
        <ol>
            {subjects.map((sub, idx) => <li key={idx}> {sub} </li>)}
        </ol>
    </div>
);

// ReactDOM can render elements
/// ReactDOM.render(element_jsx, document.getElementById('root'));


// Composition using Components
//--------------------------------

class List extends Component {
    render() {
        return <ol>{this.props.subjects.map((sub, idx) => <li key={idx}> {sub} </li>)} </ol>
    }
}

class Heading extends Component {
    render() {
        return <h1>Subjects</h1>
    }
}

// Here Main is composed of List and Header
class Main extends Component {
    render() {
        return (
            <div>
                <Heading/>
                <List subjects = {['SE', 'TFCS', 'OS', 'DBMS']}/>
                <List subjects = {['OS', 'DBMS']}/>
            </div>)
    }
}

ReactDOM.render(<Main/>, document.getElementById('root'));
